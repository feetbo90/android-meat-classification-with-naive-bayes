package classification.meat.androidmeatclassification;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;

import ProbCounter.ArrayDataEachFeature;
import ProbCounter.ArrayDataPosterior;
import ProbCounter.DataProcessing;
import ProbCounter.ImageProcessing;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;


public class MainActivity extends AppCompatActivity {

    final String path = "Data Training";
    public static final int REQUEST_CODE_CAMERA = 0012;
    public static final int REQUEST_CODE_GALLERY = 0013;
    private String [] items = {"Camera","Gallery"};

    ImageView imageView, imageTesting;
    Button btnTesting, btnTraining;

    Bitmap test, Gray, bit;
    EditText meanR, meanG, meanB, diameter, dbusuk, dsegar, dhasil;
    Button gallery;

    static File fileTesting;

    static Bitmap bimg = null;

    ImageProcessing image ;
    ImageProcessing testingImage = null;
    final DataProcessing dataPro = new DataProcessing();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageTesting = (ImageView) findViewById(R.id.imageTesting);
        imageView = (ImageView) findViewById(R.id.imageView) ;
        btnTesting = (Button) findViewById(R.id.btnTesting);
        btnTraining = (Button) findViewById(R.id.btnTraining);
        dbusuk = (EditText) findViewById(R.id.dbusuk);
        dsegar = (EditText) findViewById(R.id.dsegar);
        meanR = (EditText) findViewById(R.id.meanR);
        meanG = (EditText) findViewById(R.id.meanG);
        meanB = (EditText) findViewById(R.id.meanB);
        diameter = (EditText) findViewById(R.id.diameter);
        dhasil = (EditText) findViewById(R.id.dhasil);
        gallery = (Button) findViewById(R.id.gallery);

        testingImage = new ImageProcessing();

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImage();
            }
        });

        btnTraining.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccessImage accessImage = new AccessImage();
                accessImage.execute();
            }
        });

        btnTesting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TestingImage().execute();

            }
        });

    }

    /**
     * this method used to open image directory or open from camera
     */
    private void openImage(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Options");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(items[i].equals("Camera")){
                    EasyImage.openCamera(MainActivity.this,REQUEST_CODE_CAMERA);
                }else if(items[i].equals("Gallery")){
                    EasyImage.openGallery(MainActivity.this, REQUEST_CODE_GALLERY);
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                switch (type){
                    case REQUEST_CODE_CAMERA:


                        Glide.with(MainActivity.this)
                                .load(imageFile)
                                .centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(imageTesting);
                        //tvPath.setText(imageFile.getAbsolutePath());
                        break;
                    case REQUEST_CODE_GALLERY:
                        fileTesting = imageFile;
                        Glide.with(MainActivity.this)
                                .load(imageFile)
                                .centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(imageTesting);
                        //tvPath.setText(imageFile.getAbsolutePath());
                        break;
                }

                fileTesting = imageFile;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                try {
                    bit = BitmapFactory.decodeStream(new FileInputStream(fileTesting), null, options);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private class TestingImage extends AsyncTask<Void, Void, String>{

        @Override
        protected String doInBackground(Void... params) {

            AssetManager am = getAssets();
            InputStream bitmap = null;
            final ImageProcessing imageProTest;
            //try {
                //bitmap = am.open("Data Training/gambar4.jpg");
                //bimg = BitmapFactory.decodeStream(bitmap);
                bimg = testingImage.getResizedBitmap(bit, 80, 80);
                //
                 testingImage.setImage(bimg);
                //imageProTest = dataPro.dataTraining(bimg, "Nipis");
                String hasil = dataPro.dataTesting(bimg);
                //System.out.println(hasil);
                Log.d("class hasil : " , hasil + " " + bimg.getWidth() +  " " + bimg.getHeight());
            //} catch (IOException e) {
              //  e.printStackTrace();
            //}

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            // format decimal
            DecimalFormat df = new DecimalFormat("#.###");

            meanR.setText(df.format(testingImage.getR()));
            meanG.setText(df.format(testingImage.getG()));
            meanB.setText(df.format(testingImage.getB()));
            diameter.setText(df.format(testingImage.getDiameter()));
            String hasil = dataPro.dataTesting(testingImage.getImage());
            ArrayList<ArrayDataPosterior> allPosterior = dataPro.getSemuaPosterior();
            dbusuk.setText(""+allPosterior.get(0).getProbability());
            dsegar.setText(allPosterior.get(1).getProbability()+"");
            hasil = dataPro.dataTesting(bimg);
            dhasil.setText(hasil);
            imageTesting.setImageBitmap(bimg);
        }
    }

    private class AccessImage extends AsyncTask<Void, Void, String>{

        @Override
        protected String doInBackground(Void... params) {
            Bitmap imageInput = null;
            ImageProcessing imagePro;
            ArrayDataEachFeature valueFeature;
            int total_data_train = 13;
            int total_data_train_jn = 6;
            int total_data_train_jl = 7;
            int total_data_train_jm = 2;

            Log.d("nilai", "masuk");

            AssetManager am = getAssets();



            for(int j = 1; j<= total_data_train; j++)
            {
                try {

                    String[] fileName = {""};
                    String[] ClassFile = {""};
                    if(j <= total_data_train_jn) {


                        InputStream bitmap = am.open("Data Training/" + j + ".jpg");
                        test = BitmapFactory.decodeStream(bitmap);
                        ClassFile[0] = "Daging Busuk";


                        image = new ImageProcessing();
                        image.setImage(test);
                        Gray = image.getGrayImage();
                        //image.imageToBinary();
                        Gray = image.getBinaryImage();
                        Log.d("nilai", j + " " + test.getWidth());
                        Log.d("classku loop" + j, "Daging Busuk" + j);

                    }
                    else if(j <= (total_data_train_jn + total_data_train_jl)) {


                        InputStream bitmap = am.open("Data Training/ga" + (j -total_data_train_jn ) + ".jpg");
                        test = BitmapFactory.decodeStream(bitmap);
                        ClassFile[0] = "Daging Segar";


                        image = new ImageProcessing();
                        image.setImage(test);
                        //Gray = image.getMaxImage();
                        //Gray = image.imageToBinary();
                        Gray = image.getGrayImage();
                        //image.imageToBinary();
                        Gray = image.getBinaryImage();

                        Log.d("nilai", j + " " + test.getWidth());
                        Log.d("classku loop"+ j, "Daging Segar" + (j -total_data_train_jn ));
                    }/*
                    else  {


                        InputStream bitmap = am.open("Data Training/jerukManis" + (j - (total_data_train_jn + total_data_train_jl) + ".jpg"));
                        test = BitmapFactory.decodeStream(bitmap);
                        ClassFile[0] = "Manis";

                        //image = new ImageProcessing();
                        //image.setImage(test);
                        //Gray = image.getMaxImage();
                        //Gray = image.imageToBinary();

                        Log.d("nilai", j + " " + test.getWidth());
                        Log.d("classku loop"+ j, "Manis");

                    }*/
                    imagePro = dataPro.dataTraining(test, ClassFile[0]);
                    valueFeature = dataPro.getDataFeature();

                    // format decimal
                    DecimalFormat df = new DecimalFormat("#.###");
                    //df.format(valueFeature.red);
                    //df.format(valueFeature.green);
                    //df.format(valueFeature.blue);
                    //df.format(valueFeature.diameter);
                } catch (IOException e) {
                    System.out.println("Data Not Found.");
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {


            imageView.setImageBitmap(Gray);
        }
    }
}
