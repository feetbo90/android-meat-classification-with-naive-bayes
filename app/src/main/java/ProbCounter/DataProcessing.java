package ProbCounter;

import android.graphics.Bitmap;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by root on 12/08/17.
 **/

public class DataProcessing {

    /**
     * List Class.
     */
    public String[] class_ = {"Daging Busuk", "Daging Segar"};

    /**
     * Array dataset every class.
     */
    private ArrayList<ArrayDataEachClass> dataset;

    /**
     * ArrayDataEachFeature to save rgb value & diameter.
     */
    private ArrayDataEachFeature valueFeature;

    /**
     * Array probability posterior every class.
     */
    private ArrayList<ArrayDataPosterior> allPosterior;


    public DataProcessing(){

        //create dataset each class
        dataset = new ArrayList<>();
        for (int i = 0; i < class_.length; i++) {
            dataset.add(new ArrayDataEachClass(class_[i]));
        }

    }

    /**
     * Extraction data from image, & add to dataset
     * @param input
     * @param className
     * @return image
     */
    public ImageProcessing dataTraining(Bitmap input, String className) {
        ImageProcessing image = new ImageProcessing();

        image.setImage(input);
        double[] fitur = {image.getR(), image.getG(), image.getB()};
        this.valueFeature = new ArrayDataEachFeature(fitur[0], fitur[1], fitur[2], image.getDiameter());
        for (int i = 0; i < class_.length; i++) {
            if (class_[i].toLowerCase().equals(className.toLowerCase())) {
                //Add ArrayDataEachFeature new to dataset class_ at index i
                Log.d("classku : ", class_[i]);
                this.dataset.get(i).addData(valueFeature);
                break;
            }
        }


        Log.d("banyak class_ :" , ""+class_.length);
        Log.d("banyak class_ :", "" +class_[0]);

        for (int i = 0; i < class_.length; i++) {
            Log.d("class " ,  "class_ ke-"+i+":"+class_[i]);
            Log.d("class ", "dataset :"+dataset.get(i).getSize());
            //cl.add(new ArrayDataMeanVar(class_[i], dataset.get(i).getMatrix()));
        }
        return image;
    }


    public String dataTesting(Bitmap input)
    {
        ArrayDataEachFeature dataTesting;
        ImageProcessing imageTesting = new ImageProcessing();
        imageTesting.setImage(input);
        double[] fitur = {imageTesting.getR(),imageTesting.getG(),imageTesting.getB()};
        dataTesting = new ArrayDataEachFeature(fitur[0], fitur[1], fitur[2], imageTesting.getDiameter());
        double[] sampel = dataTesting.getMatrix();
        //Create ArrayDataMeanVar Mean & Variance
        ArrayList<ArrayDataMeanVar> cl = new ArrayList<>();

        Log.d("banyak class_ testing :" , ""+ dataset.get(1).getSize());
        Log.d("banyak class_ testing :", "" +class_[0]);
        for (int i = 0; i < class_.length; i++) {
            Log.d("class " ,  "class_ ke-"+i+":"+class_[i]);
            Log.d("class ", "dataset :"+dataset.get(i).getMatrix());
            cl.add(new ArrayDataMeanVar(class_[i], dataset.get(i).getMatrix()));
        }

        //Compute prob. likelihood
        ArrayList<ArrayDataLikelihood> lk = new ArrayList<>();
        for(int j = 0; j < class_.length; j++) {
            lk.add(new ArrayDataLikelihood(class_[j], cl.get(j), sampel));
        }

        //Compute prob. posterior
        ArrayList<ArrayDataPosterior> pt = new ArrayList<>();
        for(int k = 0; k < class_.length; k++) {
            pt.add(new ArrayDataPosterior(class_[k], lk.get(k), getProbClass(k)));
        }

        //Final decision class as result
        ArrayDataPosterior chosen = pt.get(0);
        for(int m = 1; m < class_.length; m++) {
            if(chosen.getProbability() < pt.get(m).getProbability()) {
                chosen = pt.get(m);
            }
        }

        this.allPosterior = pt;

        System.out.println("Class Final Is " + chosen.getNameClass());
        return chosen.getNameClass();
        //return null;
    }

    /**
     * find feature
     * @return
     */
    public ArrayDataEachFeature getDataFeature() {
        return this.valueFeature;
    }

    /**
     * find probability class_
     * @param
     * @return
     */
    public double getProbClass(int index) {
        double totalDataset = 0.0;
        double class_Dataset = dataset.get(index).getSize();
        for(int i = 0; i < dataset.size(); i++) {
            totalDataset += dataset.get(i).getSize();
        }
        return (double)(class_Dataset/totalDataset);
    }

    public ArrayList<ArrayDataPosterior> getSemuaPosterior() {
        return this.allPosterior;
    }



}
