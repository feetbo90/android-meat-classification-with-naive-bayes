package ProbCounter;

/**
 * Created by root on 12/08/17.
 **/

public class ArrayDataEachFeature {
    public double red;
    public double green;
    public double blue;
    public double diameter;

    public ArrayDataEachFeature(double r_in, double g_in, double b_in, double diameter_in) {
        this.red = r_in;
        this.green = g_in;
        this.blue = b_in;
        this.diameter = diameter_in;
    }

    public double[] getMatrix() {
        double[] output = {red, green, blue, diameter};
        return output;
    }



    public void printMatrix() {
        System.out.println(red + " " + green + " " + blue + " " + diameter);
    }
}
