package ProbCounter;

import android.util.Log;

/**
 * Created by root on 14/08/17.
 **/

public class ArrayDataMeanVar {

    private String nameClass;
    private int totalData;
    private int totalFeature;
    private double[] mean;
    private double[] variance;

    public ArrayDataMeanVar(String kelas, double[][] data) {
        double[] meanTemp;
        double[] varianceTemp;
        int row = 0;
        int column = 0;

        System.out.println("Length of data[0] :"+data[0].length);

        //Find mean
        meanTemp = new double[data[0].length];
        for (row = 0; row < data.length; row++) {
            for (column = 0; column < data[0].length; column++) {
                meanTemp[column] += data[row][column];
            }
        }
        for (int i = 0; i < column; i++) {
            meanTemp[i] /= row;
        }

        //Find variancece
        varianceTemp = new double[data[0].length];
        for (row = 0; row < data.length; row++) {
            for (column = 0; column < data[0].length; column++) {
                varianceTemp[column] += Math.pow((data[row][column] - meanTemp[column]),2);
            }
        }
        for (int j = 0; j < column; j++) {
            varianceTemp[j] /= (row - 1);
        }
        this.totalFeature = column;
        this.totalData = row;
        this.nameClass = kelas;
        this.mean = meanTemp;
        this.variance = varianceTemp;

        //For log
        this.printClassifier();
    }

    public double[] getVariance() {
        return this.variance;
    }

    public double[] getMean() {
        return this.mean;
    }

    public int getJumlahFeature() {
        return this.totalFeature;
    }

    public int getJumlahData() {
        return this.totalData;
    }

    public void printClassifier() {
        System.out.println("Classifier " + this.nameClass + ": ");
        this.printMean();
        this.printVariance();
        System.out.println("----------------------------");
    }

    public void printMean() {
        System.out.println(" Mean:");
        for(int i = 0; i < mean.length; i++) {
            System.out.print(mean[i] + " ");
            System.out.println("");
        }
    }

    public void printVariance() {
        System.out.println(" Variance:");
        for(int i = 0; i < variance.length; i++) {
            System.out.print(variance[i] + " ");
            System.out.println("");
        }
    }
}
