package ProbCounter;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by root on 12/08/17.
 **/

public class ImageProcessing {


    private ArrayList koordObjek;
    private Bitmap realImage;
    private Bitmap grayImage;
    private Bitmap binaryImage;
    private Bitmap maxImage;
    private Bitmap DiameterLineImage;
    private int tinggiCitra;
    private int lebarCitra;
    private double diameterObjek;
    private double xKanan;
    private double xKiri;
    private double meanR;
    private double meanG;
    private double meanB;
    private double meanRGB;
    private double varMeanRGB;

    public ImageProcessing(){
        this.realImage = null;
        this.grayImage = null;
        this.binaryImage = null;
        this.maxImage = null;
        this.meanR = 0;
        this.meanG = 0;
        this.meanB = 0;
        this.koordObjek = new ArrayList();
        this.diameterObjek = 0;
        this.DiameterLineImage=null;
        this.xKanan=0;
        this.xKiri=0;
        this.lebarCitra = 0;
        this.tinggiCitra = 0;
    }

    /** Tentukan image yang akan diproses
     *
     * @return
     **/

    public void setImage(Bitmap input)
    {
        this.realImage = input;
        this.setSize();
        this.imageToGray();
        this.imageToBinary();
        this.imageToMax();
        this.hitungDiameter();
        this.hitungMeanR_G_B();
    }

    /** Pemrosesan image menjadi grayscale
     *
     */

    public void imageToGray()
    {
        double red, green, blue;
        int gray;
        Bitmap output = Bitmap.createBitmap(lebarCitra, tinggiCitra, Bitmap.Config.ARGB_8888);

        for (int y = 0; y < tinggiCitra; y++) {
            for (int x = 0; x < lebarCitra; x++) {

//                before = new Color(realImage.getPixel(x,y)& 0x00ffffff);

                int colour = realImage.getPixel(x,y) & 0x00ffffff;

                red = Color.red(colour) * 0.2989;
                blue = Color.blue(colour) * 0.5870;
                green = Color.green(colour) * 0.1140;

                gray = (int)(red + green + blue);


                // Calculate RGB to gray
                // with lumonisity algorithm
                //red = (double) (before.getRed() * 0.2989);
                //green = (double) (before.getGreen() * 0.5870);
                //blue = (double) (before.getBlue() * 0.1140);
                //gray = (int) (red + green + blue);

                //after = new Color(gray, gray, gray);
                //output.setRGB(x, y, after.getRGB());
                output.setPixel(x,y, Color.rgb((int)(red), (int)green,(int)blue));
               // Log.d("nilai output" , ""+output.getPixel(x,y));
            }
        }
        this.grayImage = output;

    }


    /**
     *  Pemrosesan Image To Binary
     */


    public void imageToBinary()
    {
        Color before, after;
        koordObjek.clear();
        int after2 = 0;
        Bitmap output = Bitmap.createBitmap(lebarCitra, tinggiCitra, Bitmap.Config.ARGB_8888);

        for (int y = 0; y < tinggiCitra; y++) {
            for (int x = 0; x < lebarCitra; x++) {

                int colour = realImage.getPixel(x,y) & 0x00ffffff;


                if (Color.blue(colour) < 200) {
                    //after = new Color(255, 255, 255);
                    //red = Color.red(colour) * 0.2989;
                    //blue = Color.blue(colour) * 0.5870;
                    //green = Color.green(colour) * 0.1140;
                    after2 = Color.rgb(Color.red(colour), Color.green(colour), Color.blue(colour));
                    //after2 = Color.rgb(255, 255, 255);
                    koordObjek.add(String.valueOf(x) + "," + String.valueOf(y));
                } else {
                    //after2 = Color.rgb(0,0,0);
                    after2 = Color.rgb(Color.red(colour), Color.green(colour), Color.blue(colour));
                }
//                output.setRGB(x, y, after.getRGB());
                output.setPixel(x,y, after2);
            }
        }
        this.binaryImage = output;

    }


    /**
     * Proses image binary menggunakan algoritma max filter.
     */
    private void imageToMax() {
        //Size window matrix, untuk matrik konvolusi
        int size = 3; // nilainya harus >=3 dan harus ganjil

        //tambahkan 1px diluar image, untuk pengecekan
        Bitmap largerImage = addBoundary(this.binaryImage);


        //Ukuran matriks window size ^ 2
        int[] matriksWindow = new int[(int)Math.pow(size, 2)];
        Bitmap output = Bitmap.createBitmap(lebarCitra, tinggiCitra, Bitmap.Config.ARGB_8888);

        //Lakukan pengecekan nilai max per matrix window terhadap image
        for (int y = 0; y < tinggiCitra; y++) {
            for (int x = 0; x < lebarCitra; x++) {
                matriksWindow = getWindow(largerImage, x+1, y+1, size);
                output.setPixel(x, y, getMax(matriksWindow));
            }
        }
        this.maxImage = output;
    }

    /**
     * Dapatkan array of integer dari windows image koordinat x dan y.
     * @param img
     * @param x
     * @param y
     * @return
     */
    private static int[] getWindow(Bitmap img, int x, int y, int sizeWindow) {
        int[] output = new int[(int)Math.pow(sizeWindow, 2)];
        int plus = 0;
        for(int i = 0; i < sizeWindow; i++) {
            for(int j = 0; j < sizeWindow; j++) {
                output[plus] = img.getPixel((x-1)+i, (y-1)+j);
//                output[plus] = img.getRGB((x-1)+i, (y-1)+j);
                plus++;
            }
        }
        return output;
    }

    /**
     * Dapatkan max value dari array of integer.
     * @param arrayInt
     * @return
     */
    private static int getMax(int[] arrayInt) {
        int max = arrayInt[0];
        for(int i = 1; i < arrayInt.length; i++) {
            if(arrayInt[i] > max) {
                max = arrayInt[i];
            }
        }
        return max;
    }

    /**
     * Tambahkan border di luar image.
     * @param input
     * @return
     */
    private Bitmap addBoundary(Bitmap input) {
        //Buat buffered image ukuran lebar dan tinggi + 2
        Bitmap output = Bitmap.createBitmap(lebarCitra+2, tinggiCitra+2, Bitmap.Config.ARGB_8888);

        int after2 =0;


        for (int y = 0; y < tinggiCitra+2; y++) {
            for (int x = 0; x < lebarCitra+2; x++) {
                if(x == 0 || y == 0 || x == tinggiCitra+1 || y == lebarCitra+1 )
                {
                    output.setPixel(x,y, Color.rgb(0,0,0));
                }
                else if(((x-2)>=0) && ((y-2)>=0) ){
                    //Log.d("nilai" , "tinggi : " + (x-2) + " lebar " + (y-2) +" " + input.getWidth() + " " + input.getHeight());
                    int colour = input.getPixel(x-2,y-2) ;
                    if (Color.blue(colour) < 251) {
                        //after = new Color(255, 255, 255);
                        after2 = Color.rgb(255, 255, 255);
                    } else {
                        after2 = Color.rgb(0,0,0);
                    }
                    output.setPixel(x,y, after2);
                }


            }
        }



        return output;
    }


    /**
     * Dapatkan image yg asli.
     * @return
     */
    public Bitmap getImage() {
        return this.realImage;
    }

    /**
     * Dapatkan image yg sudah diproses menjadi grayscale.
     * @return
     */
    public Bitmap getGrayImage() {
        return this.grayImage;
    }

    /**
     * Dapatkan image yg sudah diproses menjadi binary image.
     * @return
     */
    public Bitmap getBinaryImage() {
        return this.binaryImage;
    }

    /**
     * Dapatkan image yg sudah di proses/max filter.
     * @return
     */
    public Bitmap getMaxImage() {
        return this.maxImage;
    }

    /**
     * Mendapatkan diameter dari objek pada gambar.
     * @return
     */
    public double getDiameter() {
        return this.diameterObjek;
    }

    public double getxKiriDiameter() {
        return this.xKiri;
    }

    public double getxKananDiameter() {
        return this.xKanan;
    }

    public Bitmap getDiameterLine() {
        return this.DiameterLineImage;
    }

    /**
     * Dapatkan mean red value dari image.
     * @return
     */
    public double getR() {
        return this.meanR;
    }

    /**
     * Dapatkan mean green value dari image.
     * @return
     */
    public double getG() {
        return this.meanG;
    }

    /**
     * Dapatkan mean blue value dari image.
     * @return
     */
    public double getB() {
        return this.meanB;
    }

    /**
     * Mendapatkan dimensi lebar dari image.
     * @return
     */
    public int getLebar() {
        return this.lebarCitra;
    }

    /**
     * Mendapatkan dimensi tinggi dari image.
     * @return
     */
    public int getTinggi() {
        return this.tinggiCitra;
    }

    /**
     * Dapatkan ukuran image, dari image asli.
     */
    private void setSize() {
        this.tinggiCitra = realImage.getHeight();
        this.lebarCitra = realImage.getWidth();
    }

    /**
     * Mendapatkan mean RGB dari objek gambar.
     *
     */
    private void hitungMeanR_G_B() {
        int length = koordObjek.size();
        double red = 0;
        double green = 0;
        double blue = 0;
        int x, y;
        int colour;
        Color before;
        String xy[];
        for (int i = 0; i < length; i++) {
            xy = koordObjek.get(i).toString().split(",");

            //System.out.println(xy[0] + "-" + xy[1]);
            x = Integer.parseInt(xy[0]);
            y = Integer.parseInt(xy[1]);
            colour = realImage.getPixel(x,y) & 0x00ffffff;

            red += Color.red(colour) * 0.2989;
            blue += Color.blue(colour) * 0.5870;
            green += Color.green(colour) * 0.1140;

        }
        red /= length;
        blue /= length;
        green /= length;

        this.meanR = red;
        this.meanG = green;
        this.meanB = blue;

        System.out.println("meanR :"+red);
    }

    /**
     * Mencari diameter dari citra, input haruslah berupa citra biner hitam
     * putih
     *
     */
    private void hitungDiameter() {
        int xKanan_hitung;
        int xKiri_hitung;
        int colour;
        //dari tepi kiri
        outloop:
        for (xKiri_hitung = 0; xKiri_hitung < lebarCitra; xKiri_hitung++) {
            for (int yKiri_hitung = 0; yKiri_hitung < tinggiCitra; yKiri_hitung++) {
                //Color c = new Color(binaryImage.getRGB(xKiri_hitung, yKiri_hitung) & 0x00ffffff);
                colour = binaryImage.getPixel(xKiri_hitung, yKiri_hitung) & 0x00ffffff;
                if (Color.green(colour)  == 255) {
                    break outloop;
                }
            }
        }

        //dari tepi kanan
        outloop2:
        for (xKanan_hitung = lebarCitra - 1; xKanan_hitung > 0; xKanan_hitung--) {
            for (int yKanan_hitung = 0; yKanan_hitung < tinggiCitra; yKanan_hitung++) {
                //Color d = new Color(binaryImage.getRGB(xKanan_hitung, yKanan_hitung) & 0x00ffffff);
                colour = binaryImage.getPixel(xKanan_hitung, yKanan_hitung) & 0x00ffffff;
                if (Color.green(colour) == 255) {
                    break outloop2;
                }
            }
        }
        //Kembalikan nilai selisih x
        this.diameterObjek = (xKanan_hitung - xKiri_hitung);
        this.xKanan=xKanan_hitung;
        this.xKiri=xKiri_hitung;


    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);
        // RECREATE THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);
        return resizedBitmap;
    }


}
