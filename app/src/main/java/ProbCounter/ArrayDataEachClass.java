package ProbCounter;

import java.util.ArrayList;

/**
 * Created by root on 12/08/17.
 **/

public class ArrayDataEachClass {

    private String nameClass;
    private ArrayList<ArrayDataEachFeature> data;
    private int sizeData;

    public ArrayDataEachClass(String name)
    {
        this.data = new ArrayList<>();
        this.sizeData = 0;
        this.nameClass = name;
    }

    public String getNameClass() {
        return this.nameClass;
    }

    /**
     * to add new data to dataset this class.
     * @param
     */
    public void addData(ArrayDataEachFeature dataBaru) {
        this.data.add(dataBaru);
        this.sizeData = this.data.size();
    }

    /**
     * find total data from dataset this class.
     * @return
     */
    public int getSize() {
        return this.sizeData;
    }

    public double[][] getMatrix(){
        int totalFeature = 4;
        int totalData = data.size();
        System.out.println("totalFeature :"+totalFeature);
        System.out.println("totalData :"+totalData);
        double[][] matrix = new double[totalData][totalFeature];
        for(int i = 0; i < totalData; i++) {
            matrix[i][0] = data.get(i).red;
            matrix[i][1] = data.get(i).green;
            matrix[i][2] = data.get(i).blue;
            matrix[i][3] = data.get(i).diameter;
        }

        //System.out.println("matrix[i][0] : "+matrix[0][0]);

        return matrix;
    }

    /**
     * Change data at dataset entire.
     * @param matrix
     */
    public void setMatrix(double[][] matrix) {
        data.clear();
        for(int i = 0; i < matrix.length; i++) {
            data.add(new ArrayDataEachFeature(matrix[i][0], matrix[i][1], matrix[i][2], matrix[i][3]));
        }
    }

    /**
     * find red value from index data.
     * @param index
     * @return
     */
    public double getRed(int index) {
        return this.data.get(index).red;
    }

    /**
     * find green value from index data.
     * @param index
     * @return
     */
    public double getGreen(int index) {
        return this.data.get(index).green;
    }

    /**
     * find blue value from index data.
     * @param index
     */
    public double getBlue(int index) {
        return this.data.get(index).blue;
    }

    /**
     * find RGB value from index data.
     * @param index
     */
    public double getDiameter(int index) {
        return this.data.get(index).diameter;
    }


}
