package ProbCounter;

/**
 * Created by root on 12/08/17.
 **/

public class ArrayDataPosterior {
    private String nameClass;
    private double ProbPosterior;

    public ArrayDataPosterior(String class_, ArrayDataLikelihood likelihood, double probClass) {
        double tempProbPosterior = probClass;

        //Kalikan semua probabilitas fitur untuk mendapatkan posterior
        for(int i = 0; i < likelihood.getPFeatureClass().length; i++) {
            tempProbPosterior *= likelihood.getPFeatureClass()[i];
        }
        this.nameClass = class_;
        this.ProbPosterior = tempProbPosterior;

        //Untuk log
        this.printProbability();
    }

    /**
     * Return value of probability
     * @return
     */
    public double getProbability() {
        return this.ProbPosterior;
    }

    /**
     * Return name class_
     * @return
     */
    public String getNameClass() {
        return this.nameClass;
    }

    /**
     * Print value probability posterior
     */
    public void printProbability() {
        System.out.println("Probability Posterior " + nameClass + " : " + this.ProbPosterior);
    }

}
